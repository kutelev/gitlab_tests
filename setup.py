from setuptools import setup

requirements = ['pytest', 'requests']

setup(
    name='gitlab',
    version='latest',
    author='Alexander Kutelev',
    author_email='alexander@kutelev.ru',
    packages=['gitlab', 'gitlab_tests'],
    description='GitLab tests',
    install_requires=requirements
)
