from base64 import b64decode, b64encode
from os import environ
from requests import post
from typing import Union, Optional, List
from urllib.parse import quote_plus, urlparse

from gitlab.session import Session, SessionException, retry


def default_rest_api_base_url() -> str:
    url = environ.get('CI_API_V4_URL', 'https://gitlab.com/api/v4')
    assert (url.startswith('https://') or url.startswith('http://')) and url.endswith('/api/v4')  # noqa: `http` is used for testing only, it's okay.
    return url


def default_hostname() -> str:
    return urlparse(default_rest_api_base_url()).netloc


def default_scheme() -> str:
    return urlparse(default_rest_api_base_url()).scheme


class GitLabSessionException(SessionException):
    pass


class GitLabSession(Session):
    def __init__(self, *, token: Optional[str] = None, username: Optional[str] = None, password: Optional[str] = None,
                 client_id: Optional[str] = None, client_secret: Optional[str] = None,
                 hostname: Optional[str] = None, use_http: bool = False, port: Optional[int] = None):
        token_provided = token is not None
        credentials_provided = username is not None and password is not None
        client_credentials_provided = client_id is not None and client_secret is not None
        assert not token_provided or not credentials_provided
        assert not token_provided or not client_credentials_provided
        assert int(credentials_provided) >= int(client_credentials_provided)
        assert (username is None) == (password is None)
        assert (client_id is None) == (client_secret is None)
        assert hostname is None or isinstance(hostname, str)
        assert isinstance(use_http, bool)
        assert port is None or isinstance(port, int)
        self.proto = default_scheme() if hostname is None else ('http' if use_http else 'https')
        self.port = port if port is not None else {'http': 80, 'https': 443}[self.proto]
        self.hostname = default_hostname() if hostname is None else hostname
        rest_api_base_url = default_rest_api_base_url() if hostname is None else f'{self.proto}://{self.hostname}:{self.port}/api/v4'
        Session.__init__(self, rest_api_base_url=rest_api_base_url)
        self.token = None
        self.authorization = None
        if token is not None:
            self.token = token
        elif username is not None and password is not None:
            self.authorization = self.get_access_token(username=username, password=password, client_id=client_id, client_secret=client_secret)
            if self.authorization is None:
                raise GitLabSessionException('Failed to request a GitLab access token')
        self.danger_zone_entered = False

    def enter_danger_zone(self):
        assert self.user['username'] == 'root', 'Only root is allowed to enter danger zone'
        self.danger_zone_entered = True

    def create_group(self, *, name: str, path: str, parent_id: Optional[int] = None) -> dict:
        assert isinstance(name, str)
        assert isinstance(path, str)
        assert parent_id is None or isinstance(parent_id, int)
        request = Session._clean_request({'name': name, 'path': path, 'parent_id': parent_id})
        return self._send_request(uri='groups', request=request, request_method=GitLabSession.RequestMethod.POST)

    @property
    def projects(self) -> List[dict]:
        project_list = self._pager(uri='projects?archived=0&simple=0')
        project_list.sort(key=lambda x: x['id'])
        return project_list

    def create_project(self, *, name: Optional[str] = None, path: Optional[str] = None, namespace_id: Optional[int] = None) -> dict:
        assert name is None or isinstance(name, str)
        assert path is None or isinstance(path, str)
        assert name is not None or path is not None
        assert namespace_id is None or isinstance(namespace_id, int)
        request = Session._clean_request({'name': name, 'path': path, 'namespace_id': namespace_id})
        return self._send_request(uri='projects', request=request, request_method=GitLabSession.RequestMethod.POST)

    def fork_project(self, *, project_id: Union[int, str], namespace: Union[int, str] = None, namespace_id: Optional[int] = None,
                     namespace_path: Optional[str] = None, name: Optional[str] = None, path: Optional[str] = None) -> dict:
        assert isinstance(project_id, (int, str))
        assert namespace is None or isinstance(namespace, (int, str))
        assert namespace_id is None or isinstance(namespace_id, int)
        assert namespace_path is None or isinstance(namespace_path, str)
        assert name is None or isinstance(name, str)
        assert path is None or isinstance(path, str)
        request = Session._clean_request({'namespace': namespace, 'namespace_id': namespace_id, 'namespace_path': namespace_path, 'name': name, 'path': path})
        return self._send_request(uri=f'projects/{quote_plus(str(project_id))}/fork', request=request, request_method=GitLabSession.RequestMethod.POST)

    def project_forks(self, *, project_id: Union[int, str]) -> List[dict]:
        assert isinstance(project_id, (int, str))
        return self._send_request(uri=f'projects/{quote_plus(str(project_id))}/forks')

    def create_fork_relation(self, *, project_id: Union[int, str], forked_from_id: Union[int, str]) -> dict:
        assert isinstance(project_id, (int, str))
        assert isinstance(forked_from_id, (int, str))
        return self._send_request(uri=f'projects/{quote_plus(str(project_id))}/fork/{quote_plus(str(forked_from_id))}',
                                  request_method=GitLabSession.RequestMethod.POST)

    def delete_fork_relation(self, *, project_id: Union[int, str]) -> bool:
        assert isinstance(project_id, (int, str))
        return self._send_request(uri=f'projects/{quote_plus(str(project_id))}/fork', request_method=GitLabSession.RequestMethod.DELETE,
                                  ret_type=GitLabSession.ReturnType.TRUE_OR_FALSE, validate_content_type=False)

    def delete_project(self, *, project_id: Union[int, str]) -> bool:
        assert self.danger_zone_entered
        assert isinstance(project_id, (int, str))
        return self._send_request(uri=f'projects/{quote_plus(str(project_id))}', request_method=GitLabSession.RequestMethod.DELETE,
                                  ret_type=GitLabSession.ReturnType.TRUE_OR_FALSE, validate_content_type=False)

    def project(self, *, project_id: Union[int, str]) -> dict:
        assert isinstance(project_id, (int, str))
        return self._send_request(uri=f'projects/{quote_plus(str(project_id))}')

    def update_project(self, *, project_id: Union[int, str], project: dict) -> dict:
        assert isinstance(project_id, (int, str))
        assert isinstance(project, dict)
        uri = 'projects/{project_id}'.format(project_id=quote_plus(str(project_id)))
        response = self._send_request(uri=uri, request=project, request_method=GitLabSession.RequestMethod.PUT)
        return response

    def create_branch(self, *, project_id: Union[int, str], branch_name: str, source_ref: str) -> dict:
        assert isinstance(project_id, (int, str))
        assert isinstance(branch_name, str)
        assert isinstance(source_ref, str)
        uri = f'projects/{quote_plus(str(project_id))}/repository/branches'
        branch = {'branch': branch_name, 'ref': source_ref}
        return self._send_request(uri=uri, request=branch, request_method=GitLabSession.RequestMethod.POST)

    def delete_branch(self, *, project_id: Union[int, str], branch_name: str) -> bool:
        assert isinstance(project_id, (int, str))
        assert isinstance(branch_name, str)
        uri = f'projects/{quote_plus(str(project_id))}/repository/branches/{quote_plus(branch_name)}'
        return self._send_request(uri=uri, request_method=GitLabSession.RequestMethod.DELETE, ret_type=GitLabSession.ReturnType.TRUE_OR_FALSE,
                                  validate_content_type=False)

    def branches(self, *, project_id: Union[int, str]) -> List[dict]:
        assert isinstance(project_id, (int, str))
        return self._pager(uri=f'projects/{quote_plus(str(project_id))}/repository/branches', trait='name')

    def check_file_existence(self, project_id: Union[int, str], branch_name: str, file_path: str) -> bool:
        response = self._send_request(uri=f'projects/{quote_plus(str(project_id))}/repository/files/{quote_plus(file_path)}?ref={quote_plus(branch_name)}',
                                      ret_type=GitLabSession.ReturnType.TRUE_OR_FALSE)
        return response

    def get_file(self, project_id: Union[int, str], branch_name: str, file_path: str, content_only: bool = True) -> Union[str, dict]:
        """
        :param project_id: project ID or full path (examples: 123, full/project/path)
        :param branch_name: branch name (examples: master, release/branch_name)
        :param file_path: file path (examples: .gitlab-ci.yml, build/dependencies.xml)
        :param content_only: return decoded file content only (string), this is a default behaviour, works for text files only.
        if content_only is set to False full default GitLab response is returned (dict):
        https://docs.gitlab.com/ee/api/repository_files.html#get-file-from-repository
        :return: file content (string) or a full information about a given file (dict)
        """
        assert isinstance(project_id, (int, str))
        assert isinstance(branch_name, str)
        assert isinstance(file_path, str)
        assert isinstance(content_only, bool)
        response = self._send_request(uri=f'projects/{quote_plus(str(project_id))}/repository/files/{quote_plus(file_path)}?ref={quote_plus(branch_name)}')
        if content_only:
            return b64decode(response['content']).decode()
        else:
            return response

    def update_file(self, project_id: Union[int, str], branch_name: str, file_path: str, content: str, commit_message: str, create: bool = False) -> dict:
        assert isinstance(project_id, int) or isinstance(project_id, str)
        assert isinstance(branch_name, str)
        assert isinstance(file_path, str)
        assert isinstance(content, str)
        assert isinstance(commit_message, str)
        assert isinstance(create, bool)
        uri = f'projects/{quote_plus(str(project_id))}/repository/files/{quote_plus(file_path)}?branch={quote_plus(branch_name)}&' \
              f'encoding=base64&commit_message={quote_plus(commit_message)}'
        response = self._send_request(uri=uri, request_method=GitLabSession.RequestMethod.POST if create else GitLabSession.RequestMethod.PUT,
                                      additional_codes=(400,), request={'content': b64encode(content.encode()).decode()})
        return response

    def commits(self, *, project_id: Union[int, str], branch_name: Optional[str] = None, pages: Optional[int] = None) -> List[dict]:
        assert isinstance(project_id, (int, str))
        assert branch_name is None or isinstance(branch_name, str)
        assert pages is None or isinstance(pages, int)
        uri = f'projects/{quote_plus(str(project_id))}/repository/commits'
        if branch_name is None:  # Return ALL commits for ALL branches.
            uri += '?all=1'
        else:  # Return commits for a given branch only.
            uri += f'?ref_name={quote_plus(branch_name)}'
        return self._pager(uri=uri, pages=pages)

    def commit(self, *, project_id: Union[int, str], branch_name: str, commit_message: str, actions: List[dict], start_branch: Optional[str] = None,
               start_sha: Optional[str] = None) -> dict:
        assert isinstance(project_id, (int, str))
        assert isinstance(branch_name, str)
        assert isinstance(commit_message, str)
        assert isinstance(actions, list)
        assert all(isinstance(action, dict) for action in actions)
        assert start_branch is None or isinstance(start_branch, str)
        assert start_sha is None or isinstance(start_sha, str)
        request = Session._clean_request({'branch': branch_name, 'commit_message': commit_message, 'start_branch': start_branch, 'start_sha': start_sha,
                                          'actions': actions})
        return self._send_request(uri=f'projects/{quote_plus(str(project_id))}/repository/commits', request_method=GitLabSession.RequestMethod.POST,
                                  request=request)

    @retry
    def get_access_token(self, *, username: str, password: str, client_id: Optional[str] = None, client_secret: Optional[str] = None) -> Optional[str]:
        if client_id is not None and client_secret is not None:
            url = f'{self.proto}://{quote_plus(client_id)}:{quote_plus(client_secret)}@{self.hostname}:{self.port}/oauth/token'
        else:
            url = f'{self.proto}://{self.hostname}:{self.port}/oauth/token'
        response = post(url=url, json={'grant_type': 'password', 'username': username, 'password': password})
        if 'Content-Type' not in response.headers or response.headers['Content-Type'] == 'application/json' or response.status_code != 200:
            return None
        try:
            response = response.json()
        except ValueError:
            return None
        if 'access_token' not in response:
            return None
        return response['access_token']

    @property
    def user(self) -> dict:
        return self._send_request(uri='user')

    @property
    def users(self) -> List[dict]:
        return self._pager(uri='users')

    def create_user(self, *, username: str, name: str, password: str, email: str) -> dict:
        assert all(isinstance(argument, str) for argument in (username, name, password, email))
        # skip_confirmation is set to True in order make a new user immediately active.
        user = {'username': username, 'password': password, 'email': email, 'name': name, 'skip_confirmation': True}
        return self._send_request(uri='users', request=user, request_method=GitLabSession.RequestMethod.POST)

    def delete_user(self, *, user_id: int, hard_delete: bool = False) -> bool:
        assert self.danger_zone_entered
        assert isinstance(user_id, int)
        assert isinstance(hard_delete, bool)
        uri = f'users/{user_id}'
        if hard_delete:
            uri += '?hard_delete'
        return self._send_request(uri=uri, request_method=GitLabSession.RequestMethod.DELETE,
                                  ret_type=GitLabSession.ReturnType.TRUE_OR_FALSE, validate_content_type=False)

    @property
    def application_settings(self) -> dict:
        return self._send_request(uri='application/settings')

    def update_application_settings(self, *, settings: dict) -> dict:
        assert isinstance(settings, dict)
        return self._send_request(uri='application/settings', request=settings, request_method=GitLabSession.RequestMethod.PUT)
