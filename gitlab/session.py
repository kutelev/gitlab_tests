from enum import Enum
from json import loads as json_loads
from requests import get, put, post, delete
from requests.exceptions import ConnectionError, ReadTimeout
from typing import Optional, Sequence, List, Dict, Any
from time import sleep


class SessionException(Exception):
    def __init__(self, message: str, status_code: Optional[int] = None):
        super().__init__(message)
        self.status_code = status_code


def retry(func):
    attempt_count = 5

    def wrapper(*args, **kwargs):
        for i in range(attempt_count + 1):
            try:
                return func(*args, **kwargs)
            except (ConnectionError, ReadTimeout) as e:
                if i < attempt_count:
                    sleep(1)
                    continue
                raise SessionException('Network request failed: {}'.format(str(e)))
    return wrapper


class Session:
    class ReturnType(Enum):
        TRUE_OR_FALSE = 0
        FULL_RESPONSE_OR_NONE = 1

    class RequestMethod(Enum):
        GET = 0
        POST = 1
        PUT = 2
        DELETE = 3

    def __init__(self, *, rest_api_base_url: str):
        assert isinstance(rest_api_base_url, str)
        self.token = None
        self.authorization = None
        self.basic_authorization = None
        self.rest_api_base_url = rest_api_base_url

    def __rest_api_url(self, sub_url: str) -> str:
        assert isinstance(sub_url, str)
        return '{}/{}'.format(self.rest_api_base_url, sub_url)

    @retry
    def _send_request(self, *, uri: str, raw_uri: bool = False, request_method: RequestMethod = RequestMethod.GET, request: Optional[dict] = None,
                      ret_type: ReturnType = ReturnType.FULL_RESPONSE_OR_NONE, additional_codes: Optional[Sequence[int]] = None,
                      validate_content_type: bool = True, extra_headers: Optional[dict] = None):
        assert extra_headers is None or isinstance(extra_headers, dict)

        if request_method == Session.RequestMethod.GET:
            method = get
        elif request_method == Session.RequestMethod.POST:
            method = post
        elif request_method == Session.RequestMethod.PUT:
            method = put
        elif request_method == Session.RequestMethod.DELETE:
            method = delete
        else:  # pragma: no cover
            assert False

        if self.token is not None:
            headers = {'Private-Token': self.token}
        elif self.authorization is not None:
            headers = {'Authorization': 'Bearer ' + self.authorization}
        elif self.basic_authorization is not None:
            headers = {'Authorization': 'Basic ' + self.basic_authorization}
        else:
            headers = {}  # No authorization at all
        if extra_headers is not None:
            headers = {**headers, **extra_headers}

        response = method(uri if raw_uri else self.__rest_api_url(uri), json=request, headers=headers, timeout=60)
        if validate_content_type:
            if 'Content-Type' not in response.headers:
                raise SessionException('Session error: {}'.format('There is no Content-Type in headers'))
            if response.headers['Content-Type'] not in ('application/json', 'application/json; charset=utf-8'):
                raise SessionException('Session error: {}'.format('Content-Type is not application/json'))

        if ret_type == Session.ReturnType.TRUE_OR_FALSE:
            if response.status_code in (200, 201, 202, 204):
                return True
            elif response.status_code == 404:
                return False

        if response.status_code not in (200, 201) + (additional_codes if additional_codes is not None else ()):
            try:
                message = ', {}'.format(json_loads(response.content.decode())['message'])
            except Exception:
                message = ''
            message = 'Session error: {}{}'.format(response.status_code, message)
            raise SessionException(message, response.status_code)
        if validate_content_type:
            return response.json()
        else:
            return response.content

    def _pager(self, *, uri: str, per_page: int = 100, pages: Optional[int] = None, trait: str = 'id') -> List[dict]:
        assert isinstance(uri, str)
        assert isinstance(per_page, int) and 0 < per_page <= 100
        assert pages is None or (isinstance(pages, int) and pages > 0)
        assert isinstance(trait, str)

        def extend_uri():
            return uri + f"{'&' if '?' in uri else '?'}per_page={per_page}&page={page_idx}"

        def is_duplicate(item):
            if item[trait] in indices:
                return True
            indices.add(item[trait])
            return False

        indices = set()
        object_list = []
        page_idx = 1
        while pages is None or len(object_list) < per_page * pages:
            response = self._send_request(uri=extend_uri())
            object_list.extend([item for item in response if not is_duplicate(item)])
            if len(response) < per_page:
                break
            page_idx += 1

        if pages is not None and len(object_list) > per_page * pages:
            object_list = object_list[:(per_page * pages)]

        return object_list

    @staticmethod
    def _clean_request(request: Dict[str, Any]) -> Dict[str, Any]:
        assert isinstance(request, dict)
        return {k: v for k, v in request.items() if v is not None}
