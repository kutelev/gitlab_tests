#!/bin/bash
set -ev
# Note: The token string must be 20 characters in length to be recognized as a valid personal access token.
# Reference: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#programmatically-creating-a-personal-access-token
gitlab-rails runner 'token = User.find_by_username("root").personal_access_tokens.create(scopes: [:api], name: "Full Access"); token.set_token("root'\''s-access-token0"); token.save!'
