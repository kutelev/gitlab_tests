#!/bin/bash
set -ev
# Install GitLab using official instructions.
apt-get update && apt-get -y upgrade
apt-get install -y curl openssh-server ca-certificates
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | bash
apt-get install -y gitlab-ce
# Make SSH working.
echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
echo root:password | chpasswd
# New Docker versions do not create `.dockerenv` anymore. GitLab expects this to exist. If it does not -> reconfiguration fails:
# ```
# execute[init q] (package::runit_sysvinit line 28) had an error: Mixlib::ShellOut::ShellCommandFailed: Expected process to exit with [0], but received '1'
# ---- Begin output of init q ----
# STDOUT:
# STDERR: Couldn't find an alternative telinit implementation to spawn.
# ---- End output of init q ----
# Ran init q returned 1
# ```
# Creating this item manually for the backward compatibility sake:
touch /.dockerenv
# GitLab tries to update some settings using sysctl, in unprivileged Docker it is not allowed.
# Replace sysctl with a stub which will silently accept everything without doing anything in fact.
echo "#\!/bin/bash" > /sbin/sysctl
# `initial_root_password` stopped suddenly worked after 13.12, working this around:
# https://gitlab.com/gitlab-org/gitlab/-/issues/32798
export GITLAB_ROOT_PASSWORD="password"
# The same for `initial_shared_runners_registration_token`.
export GITLAB_SHARED_RUNNERS_REGISTRATION_TOKEN="token"
# Set external_url to http://localhost:8888.
# Port 80 is listened by default on Windows by one of system services, 8080 is occupied by Puma/Unicorn.
# Using 8888 as one which is considered to be free.
sed -i -E "s/^(external_url)[[:space:]]+'.*'$/\1 'http:\/\/localhost:8888'/g" /etc/gitlab/gitlab.rb
{
  # Set initial root password and registration token for runners.
  echo "gitlab_rails['initial_root_password'] = \"password\""
  echo "gitlab_rails['initial_shared_runners_registration_token'] = \"token\""
  # Reduce concurrency in order to reduce memory consumption.
  echo "sidekiq['concurrency'] = 2"
  echo "puma['worker_processes'] = 2"
  # Disable not needed services in order to reduce memory consumption: Prometheus and Grafana.
  echo "prometheus_monitoring['enable'] = false"
  echo "grafana['enable'] = false"
  # Prevent Postgres from trying to allocate 25% of total memory.
  echo "postgresql['shared_buffers'] = '1MB'"
} >> /etc/gitlab/gitlab.rb
# Create a directory for custom pre-receive hooks.
mkdir -p /opt/gitlab/embedded/service/gitlab-shell/hooks/pre-receive.d
# Manually start runsvdir-start, otherwise reconfigure will stuck.
/opt/gitlab/embedded/bin/runsvdir-start &
gitlab-ctl reconfigure
# Install GitLab Runner.
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | bash
apt-get install -y gitlab-runner
# Wait for GitLab to become available.
while [[ "$(curl -s -o /dev/null -w '%{http_code}' http://localhost:8888/users/sign_in)" != "200" ]]; do sleep 1; done
# Register, configure and start GitLab Runner.
gitlab-runner register --registration-token=token --tag-list linux,shell --non-interactive --run-untagged=true --locked=false --maximum-timeout=600 --output-limit=100000 --name=runner --url=http://localhost:8888/ --executor=shell
# Set concurrent to 4 in order to defeat latencies and increase speed.
sed -i 's/\(concurrent\)[[:space:]]*=[[:space:]]*[0-9]*/\1 = 4/g' /etc/gitlab-runner/config.toml
# Set check_interval to 1 second.
sed -i 's/\(check_interval\)[[:space:]]*=[[:space:]]*[0-9]*/\1 = 1/g' /etc/gitlab-runner/config.toml
# Set log_level to "panic".
sed -i '1ilog_level = "panic"' /etc/gitlab-runner/config.toml
# Starting from GitLab 13.4 any newly created instance has the following group: "GitLab Instance".
# With the following project within: "Monitoring".
# Reference: https://docs.gitlab.com/ee/administration/monitoring/gitlab_self_monitoring_project/
# This group and project only complicate testing procedures.
# Schedule self-monitoring project for deletion, we do not need this during running the integration tests.
gitlab-rails r "SelfMonitoringProjectDeleteWorker.perform_async"
gitlab-runner start
