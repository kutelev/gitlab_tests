from pytest import mark

from gitlab_tests.real_gitlab import setup_function, teardown_function  # noqa: These imports are really required.
from gitlab_tests.real_gitlab import create_temp_project, create_temp_user, create_temp_group
from gitlab_tests.real_gitlab import RootSession, UserSession


@mark.parametrize('attribute_name', ('id', 'path_with_namespace'))
def test_fork_project(attribute_name: str):
    def check_fork_relation(should_exist: bool):
        forks = root_session.project_forks(project_id=original_project[attribute_name])
        assert isinstance(forks, list)
        assert len(forks) == int(should_exist)
        if should_exist:
            assert forks[0][attribute_name] == forked_project[attribute_name]

    root_session = RootSession()
    user = create_temp_user(root_session)
    user_session = UserSession(username=user['username'])
    group = create_temp_group(user_session)
    original_project = create_temp_project(root_session)
    # Change visibility to internal otherwise the user will not be able to access root's project.
    root_session.update_project(project_id=original_project[attribute_name], project={'visibility': 'internal'})
    # Checking that the user can access root's project.
    assert original_project[attribute_name] in {project[attribute_name] for project in user_session.projects}

    forked_project = user_session.fork_project(project_id=original_project[attribute_name], namespace=group['path'])
    assert isinstance(original_project, dict)
    assert isinstance(forked_project, dict)
    assert original_project['path_with_namespace'] == f"root/{original_project['path']}"
    assert forked_project['path_with_namespace'] == f"{group['path']}/{forked_project['path']}"

    # Check for fork relation existence.
    check_fork_relation(True)

    # Delete fork relation.
    user_session.delete_fork_relation(project_id=forked_project[attribute_name])
    check_fork_relation(False)

    # Restore earlier broken fork relation.
    user_session.create_fork_relation(project_id=forked_project[attribute_name], forked_from_id=original_project[attribute_name])
    check_fork_relation(True)
