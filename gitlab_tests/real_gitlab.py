from typing import Optional, List
from uuid import uuid4
from time import sleep
from platform import system
from os.path import exists, isfile

from gitlab.gitlab import GitLabSession as OriginalGitLabSession


assert system() == 'Linux', 'These tests can be run on Linux only'
# Not 100% reliable but still better than nothing.
assert exists('/.dockerenv') and isfile('/.dockerenv'), 'These tests are supposed to be run in Docker only'


hostname = 'localhost'
port = 8888
root_password = 'password'
root_token = "root's-access-token0"  # It shall be 20 chars long.
user_password = 'password'


class TokenBasedRootSession(OriginalGitLabSession):
    def __init__(self):
        super().__init__(token=root_token, hostname=hostname, use_http=True, port=port)


class GitLabSession(OriginalGitLabSession):
    def __init__(self, *, token: Optional[str] = None, username: Optional[str] = None, password: Optional[str] = None):
        super().__init__(token=token, username=username, password=password, hostname=hostname, use_http=True, port=port)


class RootSession(GitLabSession):
    def __init__(self):
        super().__init__(username='root', password=root_password)


class UserSession(GitLabSession):
    def __init__(self, *, username: str, password: str = user_password):
        assert isinstance(username, str) and isinstance(password, str)
        super().__init__(username=username, password=password)


def real_users(users: List[dict]) -> List[dict]:
    special_users = {'ghost'}
    return [user for user in users if user['username'] not in special_users]


def setup_function() -> None:
    session = TokenBasedRootSession()
    cleanup(session)
    assert len(session.projects) == 0


def cleanup(session: TokenBasedRootSession) -> None:
    session.enter_danger_zone()
    for project in sorted(session.projects, key=lambda x: x['id'], reverse=True):
        session.delete_project(project_id=project['id'])
    while len(session.projects) != 0:
        sleep(1)
    for user in filter(lambda x: x['username'] != 'root', real_users(session.users)):
        session.delete_user(user_id=user['id'])
    while len(real_users(session.users)) != 1:
        sleep(1)


def teardown_function() -> None:
    session = TokenBasedRootSession()
    cleanup(session)


def create_temp_project(session: OriginalGitLabSession, name: Optional[str] = None, namespace_id: Optional[int] = None) -> dict:
    if name is None:
        name = uuid4().hex
    project = session.create_project(name=name, namespace_id=namespace_id)
    session.update_project(project_id=project['id'], project={
        'auto_cancel_pending_pipelines': 'disabled',  # Prevent pipelines from being canceled by GitLab, it may interfere tests results a negative way.
        'build_git_strategy': 'clone',  # Use clone policy explicitly to guarantee proper working copy construction after project recreation.
        'ci_default_git_depth': 0,  # Prevent shallow clone from being attempted.
    })
    assert isinstance(project, dict)
    assert 'id' in project
    return project


def create_temp_group(session: OriginalGitLabSession, parent_id: Optional[int] = None) -> dict:
    name = uuid4().hex
    group = session.create_group(name=name, path=name, parent_id=parent_id)
    assert isinstance(group, dict)
    assert 'id' in group
    return group


def create_temp_user(session: OriginalGitLabSession) -> dict:
    return session.create_user(username=uuid4().hex, password=user_password, name='User Name', email='username@localhost')
