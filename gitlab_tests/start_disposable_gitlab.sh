#!/bin/bash
set -ev
# Manually start runsvdir-start, otherwise reconfigure will stuck.
/opt/gitlab/embedded/bin/runsvdir-start &
gitlab-ctl reconfigure
# Start GitLab.
gitlab-ctl start
# Wait for GitLab to become available.
while [[ "$(curl -s -o /dev/null -w '%{http_code}' http://localhost:8888/users/sign_in)" != "200" ]]; do sleep 1; done
# Start GitLab Runner.
gitlab-runner start
