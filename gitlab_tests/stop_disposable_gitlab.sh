#!/bin/bash
set -ev
# Stop everything
/opt/gitlab/embedded/bin/runsvdir-start &
gitlab-runner stop
gitlab-ctl stop
