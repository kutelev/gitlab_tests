#!/bin/bash
set -ev
# Upgrade the system.
apt-get update && apt-get -y upgrade
# Install required packages.
apt-get install -y curl git subversion python-pip python3-pip python3-requests python3-yaml python3-venv sqlite3
python3 -m pip install pip pytest pytest-cov pytest-split-tests lxml --upgrade
# Additional modules for static code analysis.
python3 -m pip install radon mccabe flake8 flake8_polyfill
# Configure Git, just to be capable of interacting with Git CLI.
git config --global user.email "alexander@kutelev.ru"
git config --global user.name "Alexander Kutelev"
# Configure time zone.
export DEBIAN_FRONTEND=noninteractive
apt-get install -y tzdata
echo "Asia/Tomsk" > /etc/timezone
ln -fs /usr/share/zoneinfo/Asia/Tomsk /etc/localtime
dpkg-reconfigure -f noninteractive tzdata
